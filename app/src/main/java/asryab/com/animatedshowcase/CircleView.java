package asryab.com.animatedshowcase;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

public class CircleView extends View {
    private float cx;
    private float cy;
    private float radius;
    private Paint paint;
    private int mMaxSize;
    private int mNum;
    private long mOffSet;
    private long mDuration = 200;

    public CircleView(Context context) {
        super(context);
        intiPaint();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(
                resolveSize(mMaxSize, widthMeasureSpec),
                resolveSize(mMaxSize, heightMeasureSpec));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        cx = mMaxSize;
        cy = mMaxSize;
        canvas.drawCircle(cx / 2, cy / 2, radius, paint);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        startAnimating();
    }

    private void startAnimating() {
        clearAnimation();
        final ScaleAnimation scaleAnimation = new ScaleAnimation(0.7f, 1.0f, 0.7f, 1.0f,
                ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setInterpolator(new AccelerateInterpolator());
        scaleAnimation.setDuration(mDuration);
//        scaleAnimation.setStartOffset(mOffSet*mNum);
        scaleAnimation.setRepeatCount(Animation.INFINITE);
        scaleAnimation.setRepeatMode(Animation.REVERSE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startAnimation(scaleAnimation);
            }
        }, mOffSet * mNum);
//        startAnimation(scaleAnimation);


    }

    private void intiPaint() {
        paint = new Paint();
        paint.setColor(Color.argb(90, Color.red(Color.LTGRAY), Color.green(Color.LTGRAY), Color.blue(Color.LTGRAY)));
        paint.setStyle(Paint.Style.FILL);

    }

    public void setDiametr(int diametr) {
        radius = diametr / 2;
    }

    public void setMaxSize(int maxSize) {
        mMaxSize = maxSize;
    }

    public void setNum(int num) {
        mNum = num;
    }

    public void setMaxNum(int maxNum) {
        mOffSet = mDuration / maxNum;
    }

    public void setDuration(long duration) {
        mDuration = duration;
    }
}
