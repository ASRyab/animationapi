package asryab.com.animatedshowcase;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private RelativeLayout relativeLayout;
    private ImageView imageView;
    private TextView textView;
    private AnimationAPI animationAPI1;
    private AnimationAPI animationAPI2;

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            animationAPI1.startAnimation();
            animationAPI2.startAnimation();

        }
    };
    private AnimationCircleAPI animationCircleAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        relativeLayout = (RelativeLayout) findViewById(R.id.activity_main);
        imageView = new ImageView(this);
        imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher));
        textView = new TextView(this);
        textView.setText("Example");


//        firstAttempt();

        secondAttempt();

    }

    private void secondAttempt() {
//

        animationCircleAPI = new AnimationCircleAPI(relativeLayout,imageView,4,300);

    }

    private void firstAttempt() {
        textView = new TextView(this);
        textView.setText("Example");

        relativeLayout.addView(imageView);

        relativeLayout.addView(textView);

        animationAPI1 = null;
        animationAPI2 = null;
        try {
            animationAPI1 = new AnimationAPI(imageView);
            animationAPI2 = new AnimationAPI(textView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        animationAPI1.addAnimation(2.0f, 300)
                .addAnimation(-1.2f, 200)
                .addAnimation(0.5f, 100)
                .addAnimation(1.5f, 600);


        animationAPI2.addAnimation(2.0f, 300)
                .addAnimation(1.5f, 600);

        imageView.setOnClickListener(clickListener);
        textView.setOnClickListener(clickListener);
    }
}
