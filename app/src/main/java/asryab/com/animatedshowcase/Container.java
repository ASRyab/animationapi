package asryab.com.animatedshowcase;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;

public class Container extends FrameLayout {
    private final int num;
    private final View view;
    private final long duration;
    private int maxSize;

    public Container(Context context, int num, View view, long duration) {
        super(context);
        this.duration = duration;
        this.num = num;
        this.view = view;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int height = view.getMeasuredHeight();
        int width = view.getMeasuredWidth();
        maxSize = (int) Math.sqrt(((height * height) + (width * width)));
        if (maxSize > 0) {
            initAnimation();
            int childWidthMeasureSpec = MeasureSpec.makeMeasureSpec(maxSize, MeasureSpec.AT_MOST);
            int childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(maxSize, MeasureSpec.AT_MOST);

            final int count = getChildCount();
            for (int i = 0; i < count; i++) {
                final View child = getChildAt(i);
                child.measure(childWidthMeasureSpec, childHeightMeasureSpec);

            }
            setMeasuredDimension(
                    resolveSize(maxSize, widthMeasureSpec),
                    resolveSize(maxSize, heightMeasureSpec));
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    private void initAnimation() {
        removeCircleViews();
        int dMaxSize = maxSize / num;
        if (maxSize > 0)
            for (int i = 1; i <= num; i++) {
                CircleView circleView = new CircleView(getContext());
                circleView.setDiametr(dMaxSize * i);
                circleView.setDuration(duration);
                circleView.setMaxSize(maxSize);
                circleView.setMaxNum(num);
                circleView.setNum(i);
                addView(circleView);
            }
    }

    private void removeCircleViews() {
        int childCount = getChildCount();
        if (childCount > 1)
            for (int i = childCount - 1; i > 0; i--) {
                CircleView childAt = (CircleView) getChildAt(i);
                childAt.clearAnimation();
                removeView(childAt);
            }
    }
}
