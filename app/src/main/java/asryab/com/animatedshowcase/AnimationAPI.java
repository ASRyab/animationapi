package asryab.com.animatedshowcase;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;

public class AnimationAPI {
    private final AnimationSet animationSet;
    private final View view;
    private final Context context;

    public AnimationAPI( View view) throws Exception {
        if (view == null ) {
            throw new Exception();
        }
        this.view = view;
        context = view.getContext();

         animationSet = new AnimationSet(false);

        animationSet.setFillAfter(true);
        animationSet.setInterpolator(context, android.R.anim.bounce_interpolator);

    }

    public AnimationAPI addAnimation(float size, long duration) {


        if (duration > 0 && size > 0) {
            ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, size, 1.0f, size,
                    ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                    ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
            scaleAnimation.setInterpolator(new AccelerateInterpolator());
            scaleAnimation.setDuration(duration);
            scaleAnimation.setRepeatCount(Animation.INFINITE);
            scaleAnimation.setRepeatMode(Animation.REVERSE);

            animationSet.addAnimation(scaleAnimation);
        }
        return this;
    }

    public AnimationAPI setWidth(int width) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = layoutParams.width = width;
        view.setLayoutParams(layoutParams);
        return this;
    }

    public AnimationSet getAnimationSet() {
        startAnimation();
        return animationSet;
    }

    public void startAnimation(){
        view.startAnimation(animationSet);
    }
}
