package asryab.com.animatedshowcase;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

public class AnimationCircleAPI {

    public AnimationCircleAPI(ViewGroup relativeLayout, View view, int num, long duration) {
        Container child = new Container(relativeLayout.getContext(),num,view, duration);
        relativeLayout.addView(child);
        child.addView(view);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
        layoutParams.gravity = Gravity.CENTER;
        view.setLayoutParams(layoutParams);
    }


}
